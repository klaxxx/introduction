<details><summary>Click to expand</summary>

</details>

<h5><div align="right">

_AEROSPACE DESIGN PROJECT, EAS4947_<br>
_SEMESTER 1 2021/22_<br>
</div></h5>

<div align="center"><img src="Image/Logo.png" width="500" height = "230"></img></div>

**Name**: Mohammad Haikal bin Yusoff<br>
**Matric No**: 197565

## Table Of Content


<div align="center">

- [**WEEK 1**](#log-book-01)
- [**WEEK 2**](#log-book-02)
- [**WEEK 3**](#log-book-03)
- [**WEEK 4**](#log-book-04)
- [**WEEK 5**](#log-book-05)
- [**WEEK 6**](#log-book-06)
- [**WEEK 7**](#log-book-07)
- [**WEEK 8**](#log-book-08)
- [**WEEK 9**](#log-book-09)
- [**WEEK 10**](#log-book-10)
- [**WEEK 11**](#log-book-11)
- [**WEEK 12**](#log-book-12)
- [**WEEK 13**](#log-book-13)
- [**WEEK 14**](#log-book-14)

</div>

----

<div align="right"><b> 22<sup>nd</sup> October 2021</b></div>

<h1><div align="center"> LOG BOOK 01 </div></h1>

## Agenda and Goals
* Introduction of IDP Project
* Our goals is to fly automated airship with sprayer

## Problem and descision taken
- Our Class is being divided into a subsystem which containing of<br>
    - Design and Structure
    - Propulsion and Power
    - Flight System Integration
    - Software and Control System
    - Simulation
    - Payload Design (Crop Sprayer) 

<div align="justify">
Each subsystem will collaborate with the other subsystem so that if any issues arise, the other subsystem can assist in completing this project. Aside from that, each team is made up of seven people, and each assignment completed in class is discussed in this group. The reason our class has been divided so that each of us can focus on our main project task and see it through to completion.
</div>

## Impact
* The impact on this project is everyone will have a clear vision and specific task on each subsystem to be complete.

## Next step
* To complete the given task.
* To work with other subsystem so that will have a better idea on this project.

[**Table of Content**](#table-of-content)

---
<div align="right"><b> 29<sup>th</sup> October 2021</b></div>

<h1><div align="center"> LOG BOOK 02 </div></h1>

## Agenda and Goals

* Gitlab Activity (26 November 2021), Budget listing and project timeline (29 November 2021).<br>
* To complete the gitlab activity and contribute in project timeline. (29/11/2021)

## Problem and descision taken

| Problem | Solution |
| ------ | ------ |
| It will be a lot of gantt chart if all subsystem make one for each | Create and maintanance gantt chart that contain all subsystem |
| Will be noisy to discuss all subsystem in one voice chat | Create Voice chat for all sub-system for them easy to discuss |

<div align="justify">
The reason to create the main gantt chart is want to sync the outcome of the gantt chart activity for each subsystem, and for future use other subsystem can planning the progress how it will be going. For the second problem, it's easy for each subsystem to discuss and make planning on their part and it will be messy to discuss all subsystem in one voice chat.
</div>

## Impact
<!--point out the keberkesanan-->
* Will have better and clear vision on our project.
* Other subsystem can have clear discussion and make their plan on this project.

## Next step

* Wanted to help and participate on the making of the airship.
* Improving understanding on this project.

[**Table of Content**](#table-of-content)

---
<div align="right"><b> 05<sup>th</sup> November 2021</b></div>

<h1><div align="center"> LOG BOOK 03 </div></h1>

## Agenda and Goals
They were given two task on this week
* 5-mins update on the progress on the gantt chart.
* Engineering Logbook. (team part)

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| Flow of the logbook might get mess | Give a solution on flow of the logbook part |

<div align="justify">
The reason for having a pleasant flow of the logbook is to make things easy for both the maintenance and the developer so that the maintainer does not have to merge everyone's logbook. Developers may simply change their own introductory repo to include a logbook. Since the first action is to create our own project that includes our introduction and place a link to it in the IDP repo.
</div>

## Impact
In terms of the implications of the choice, both the maintainer and the developer can have flexibility in their own repo. The logbook file is inserted into the introductory project.

## Next step
- Help other group to solve their problem
- assist those in need regarding their own gitlab repo


[**Table of Content**](#table-of-content)

---
<div align="right"><b> 12<sup>th</sup> November 2021</b></div>
<h1><div align="center"> LOG BOOK 04 </div></h1>

## Agenda and Goals
* To find Drag of the airship using trolly

<div align="center"><img src="Image/Week4.jpeg" width="300"></img>
<img src="Image/Week4_1.jpeg" width="300"></img>

_Figure 4.1 & 4.2: Setting up the airship for Drag test_<br></div>

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| To find Drag of the airship | Using trolly to find the drag of the airship |
| The attendance flow and team duties become a little jumbled | Make an excel for the attendance flow and team duties for easier to monitor |

<div align="justify">
Discuss with the propulsion team how to obtain the drag of the airship using a trolly, as this is the cheapest approach to find the drag. Another technique is to pump helium into the blimp and place weight on it so that it stays at one location and then pull it a little bit to get the reading, but because helium is expensive, we came up with the trolly concept. In terms of the team, we wanted to make the flow of attendance and duties much more seamless, therefore we created an excel spreadsheet that we can simply monitor.
</div>
## Impact
* The drag value we get from trolly concept might not accurate since the velocity might not constant along the experiment begin.
* The flow for attendance and team roles is more easier to monitor.

## Next step
* Might help other subsystem on this project meanwhile waiting for the item to arrive. 
<!--Bincang on nk cari Drag using trolly, attach gambar, solve for flow of attendance and roles for everyone, -->

[**Table of Content**](#table-of-content)

---
<div align="right"><b> 19<sup>th</sup> November 2021</b></div>
<h1><div align="center"> LOG BOOK 05 </div></h1>

## Agenda and Goals
* Making an excel for table airship design calculation
* Doing so changes on integration repo
* Help design team on measure the airship

<div align="center"><img src="Image/Week5.jpeg" width="300" height="450"></img>

_Figure 5.1: Measuring the airship_<br></div>

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| Lack of parameter required | Make a table in excel for those parameter |

<div align="justify">
By making the table for those parameter required we can have those parameter arranged in one place so that other subsystem can refer to it. 
</div>

## Impact
* Easier for other subsystem to refer on parameter required

## Next step
* Doing some changes on flight integration repo
* Help other subsystem
<!--(Making an excel for table airship design calculation: insert link excel.)-->

[**Table of Content**](#table-of-content)

---
<div align="right"><b> 26<sup>nd</sup> November 2021</b></div>
<h1><div align="center"> LOG BOOK 06 </div></h1>

## Agenda and Goals
* Helping team propulsion on calibrate and testing battery.
* Doing some update on table airship design.

<div align="center"><img src="Image/Week6_1.jpeg" width="500"></img>
<img src="Image/Week6_2.jpeg" width="500"></img>

_Figure 6.1 & 6.2: Setup the motor for calibrate and testing with battery_<br></div>

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| The esc not functioning | replace with functioning esc |
| The propller being tested upside down | replace with the right propller |

## Impact
* Team propulsion can continue their work on testing other part of the airship.
* The data in the table can be used for other subsystem.

## Next step
* Helping other subsystem on finding the parameter required.
* Might get to start testing new part of the airship.
<!--(Help team propulsion on their testing on b3 part, doing some update on table airship design calculation. (presenter of the subsystem this week))-->

[**Table of Content**](#table-of-content)

---
<div align="right"><b> 10<sup>nd</sup> December 2021</b></div>
<h1><div align="center"> LOG BOOK 07 </div></h1>

## Agenda and Goals
* To locate the delivered item at safe place

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| The delivered item was exposed at public area and might lost | The item was place at Mr Azizi office |

## Impact
* The item was safe from stolen, and the Control group could begin calibrating the airship so that it could be assembled.

## Next step
* Ready to assembled the item into airship.
<!--simpan dan susun brang-->

[**Table of Content**](#table-of-content)

---
<div align="right"><b> 17<sup>nd</sup> December 2021</b></div>
<h1><div align="center"> LOG BOOK 08 </div></h1>

## Agenda and Goals
* Present the progress of Flight System Integration.
* Make a checklist before, after and between the airhip being fly.

<div align="center"><img src="Image/Week8_1.jpeg"></img>

_Figure 8.1 :IDP Flight Test Checklist, Main Checklist_ <br>

<img src="Image/Week8_2.jpeg"></img>

_Figure 8.2 :IDP Flight Test Checklist, Equipment Checklist_<br></div>

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| The report templete may not be possible in gitlab since the CSS in gitlab is limited to a specific point. | Instead of the format itself, the flow of the report was used. |
| The procedure for launching the airship was disorganised. | As a result, a checklist was created to ensure that the flow was orderly. |

## Impact
* To making sure all the precaution were taken before, after and between the airship being fly.

## Next step
* Start on soldering some of the item.
<!--present and cari report template, Make a checklist for operation -->

[**Table of Content**](#table-of-content)

---
<div align="right"><b> 24<sup>nd</sup> December 2021</b></div>
<h1><div align="center"> LOG BOOK 09 </div></h1>

## Agenda and Goals
* Soldering the wire from esc with banana connecter.
* Make a new connecter for the battery

<div align="center"><img src="Image/Week9_4.jpg" height="350" ></img>

_Figure 9.1 :Soldering the connector_ <br>

<img src="Image/Week9_5.jpg" height="350"></img>

_Figure 9.2 :Soldering the connector_<br>

<img src="Image/Week9_6.jpg" height="350"></img>

_Figure 9.3 :Soldering the connector_<br></div>

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| The given Lipo Battery connecter were small. | Make a new connecter for the battery. |
| The banana connecter and heat shrink were insufficient. | Buy the banana connecter and heat shrink. |

## Impact
* The current Lipo Battery can connect into the board.
* The connection from ESC to motor can be done.

## Next step
* Soldering the remaining part of the connection.
<!--Soldering part -->

[**Table of Content**](#table-of-content)

---
<div align="right"><b> 31<sup>st</sup> December 2021</b></div>
<h1><div align="center"> LOG BOOK 10 </div></h1>

## Agenda and Goals
* Soldering the remaining part of the connection

<div align="center"><img src="Image/Week10_1.jpeg" width="300" height='300'></img>

_Figure 10.1 :Connection from ESC to motor_<br>

<img src="Image/Week10_2.jpeg" width="300" height='300'></img>

_Figure 10.2 :Connection from ESC to motor for control group testing PSHAU firmware._<br></div>

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| The extension wire were not enough | Buy the remaining wire |

## Impact
* The Control Group can done the testing PSHAU firmware.

## Next step
* Purchase the inadequate wire. 
* Complete the remaining soldering.
<!--Soldering part -->

[**Table of Content**](#table-of-content)

---
<div align="right"><b> 07<sup>nd</sup> January 2022</b></div>
<h1><div align="center"> LOG BOOK 11 </div></h1>

## Agenda and Goals
* Buy the insufficient wire.
* Finish the remaining part of the soldering extension wire.

<div align="center"><img src="Image/Week11_2.jpeg" width="450" height='450'></img>

_Figure 11.1 :Reduce the heat shrinkage._<br>

<img src="Image/Week11_4.jpeg" width="450" height='450'></img>

_Figure 11.2 :The extension wire was soldered and heat shrink was applied._<br>

<img src="Image/Week11_3.jpeg" width="450" height='450'></img>

_Figure 11.3 :All of the extension wires had been soldered._<br></div>

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| Wire Multicore AWG 16 were sold out | Buy Wire Multicore AWG 14 |

## Impact
* The reason Wire AWG 14 were being choose is because it can resist the same and more load as AWG 16.

## Next step
* Assembly and test the airship.
<!--Soldering part -->

[**Table of Content**](#table-of-content)

---

<div align="right"><b> 14<sup>th</sup> January 2022</b></div>
<h1><div align="center"> LOG BOOK 12 </div></h1>

## Agenda and Goals
* Helping team gondola on drill.
* Find a support for tent.

<div align="center"><img src="Image/Week12.jpeg" width="450" height='450'></img>

_Figure 12.1 :Using a drill to make hole at gondola_<br></div>

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| Tent for putting the airship might not strong enough to support the wind current | Find and make a support to the tent |

## Impact
* The tent might not easily collapse since its has a strong support to hold the tent. 

## Next step
* Attach all the component into gondola testing it with servo.

[**Table of Content**](#table-of-content)

---

<div align="right"><b> 21<sup>st</sup> January 2022</b></div>
<h1><div align="center"> LOG BOOK 13 </div></h1>

## Agenda and Goals
* Make a new connecter to BEC.
* Attach the component into gondola.
* Check the airship.

<div align="center"><img src="Image/Week13.jpeg" width="300" height='450'></img>

_Figure 13.1 :Inflated the airship to check the leaking._<br>

<img src="Image/Week13_2.jpeg" width="350" height='500'></img>

_Figure 13.2 :make a new connector for BEC._<br>

<img src="Image/Week13_1.jpeg" width="450" height='450'></img>

_Figure 13.3 :Attach the BEC to board and servo._<br>

<img src="Image/Week13_3.jpeg" width="450" height='450'></img>

_Figure 13.4 :Attach all the component into gondola._<br></div>

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| The air in the airship might have some leaking on it | Make some checking on the airship to find the leak |

## Impact
* can reduce the possibility of the air from leak. 

## Next step
* Run test on servo and motor.
* Checking the connectivity between the board.
* Fly the airship.

[**Table of Content**](#table-of-content)

---

<div align="right"><b> 28<sup>th</sup> January 2022</b></div>
<h1><div align="center"> LOG BOOK 14 </div></h1>

## Agenda and Goals
* Fly the airship.
* Make some testing on servo and motor.
* Checking the connectivity between the board.

<div align="center"><img src="Image/Week13_4.jpeg" width="450" height='450'></img>

_Figure 14.1 :Testing the servo and motor._<br>

<img src="Image/Week14.jpeg" width="450" height='450'></img>

_Figure 14.2 :Uploading the firmware into the board._<br>

<img src="Image/Week14_1.jpeg" width="650" height='450'></img>

_Figure 14.3 :Find the actual weight of the airship._<br>

<img src="Image/Week14_3.jpeg" width="450" height='450'></img>

_Figure 14.4 :The airship ready to fly._<br></div>

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| To prevent the short circuit | Checking the connectivity between the board |
| The airship torn when inserted the helium gas | Making sure the tube not touch the airship surface |

## Improvement
* Making sure all the connectivity were checked before fly the airship.
* Testing the servo and motor before attach all the component into airship.

[**Table of Content**](#table-of-content)

---
